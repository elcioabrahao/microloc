package br.com.qpainformatica.microloc.domain.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by elcio on 16/11/17.
 */

public class User extends SugarRecord  implements Serializable {


    private String numeroDocumento;
    private String nome;
    private String foto;
    private String registroAluno;

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getRegistroAluno() {
        return registroAluno;
    }

    public void setRegistroAluno(String registroAluno) {
        this.registroAluno = registroAluno;
    }
}
