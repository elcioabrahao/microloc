package br.com.qpainformatica.microloc.ui.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Beacon;
import br.com.qpainformatica.microloc.domain.model.Local;
import br.com.qpainformatica.microloc.ui.fragments.LocalFragment.OnListFragmentInteractionListener;
import br.com.qpainformatica.microloc.ui.fragments.dummy.DummyContent.DummyItem;

import java.util.List;

public class MyLocalRecyclerViewAdapter extends RecyclerView.Adapter<MyLocalRecyclerViewAdapter.ViewHolder> {

    private final List<Local> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyLocalRecyclerViewAdapter(List<Local> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_local, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getIdentificacao());
        holder.mCampusView.setText(mValues.get(position).getCampus());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mCampusView;
        public Local mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mCampusView = (TextView) view.findViewById(R.id.campus);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCampusView.getText() + "'";
        }
    }

    public void updateList(List<Local> newlist) {
        mValues.clear();
        mValues.addAll(newlist);
        this.notifyDataSetChanged();
    }
}
