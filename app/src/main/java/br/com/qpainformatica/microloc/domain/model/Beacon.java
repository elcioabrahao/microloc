package br.com.qpainformatica.microloc.domain.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by elcio on 16/11/17.
 */

public class Beacon extends SugarRecord implements Serializable {

    private String identification;
    private String menor;
    private String maior;

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getMenor() {
        return menor;
    }

    public void setMenor(String menor) {
        this.menor = menor;
    }

    public String getMaior() {
        return maior;
    }

    public void setMaior(String maior) {
        this.maior = maior;
    }

}
