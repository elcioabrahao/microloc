package br.com.qpainformatica.microloc.ui.activities;

import android.os.Handler;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dzaitsev.android.widget.RadarChartView;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.qpainformatica.microloc.R;

import static android.graphics.Paint.Style.FILL;

public class RadarActivity extends AppCompatActivity implements BeaconConsumer {

    protected static final String TAG = "RangingActivity";
    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
    private Map<String, Float> axis;
    private RadarChartView chartView;
    private String bcId;
    private BigDecimal number;
    private int mInterval = 10000; // 5 seconds by default, can be changed later
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radar);


        beaconManager.bind(this);

        axis = new LinkedHashMap<>(10);
        axis.put("1", 0.0F);
        axis.put("2", 0.0F);
        axis.put("3", 0.0F);
        axis.put("4", 0.0F);
        axis.put("5", 0.0F);
        axis.put("6", 0.0F);
        axis.put("7", 0.0F);
        axis.put("8", 0.0F);
        axis.put("9", 0.0F);

        chartView = (RadarChartView) findViewById(R.id.radar_chart);
        chartView.setAxis(axis);
        chartView.setAxisMax(3.0F);


        chartView.setAutoSize(false);
        //chartView.setCirclesOnly(true);
        //chartView.setChartStyle(FILL);

        mHandler = new Handler();
        startRepeatingTask();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
        beaconManager.unbind(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(false);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {


                for(Beacon b: beacons){

                    List<br.com.qpainformatica.microloc.domain.model.Beacon> blList = br.com.qpainformatica.microloc.domain.model.Beacon.findWithQuery(br.com.qpainformatica.microloc.domain.model.Beacon.class,"SELECT * from Beacon WHERE identification = '"+b.getId1().toString()+"'");
                    if(blList.size()>0){
                        br.com.qpainformatica.microloc.domain.model.Beacon bl = blList.get(0);
                        bcId=""+bl.getId();
                    }else{
                        bcId="UK";
                    }
                    number = new BigDecimal(b.getDistance());
                    runOnUiThread(new Runnable() {
                        public void run() {
                            chartView.addOrReplace(bcId,number.floatValue() );

                        }
                    });

                }


            }

        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {   }
    }

    private void logToDisplay(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
//                EditText editText = (EditText)RangingActivity.this.findViewById(R.id.rangingText);
//                editText.append(line+"\n");
            }
        });
    }


    public void setDistancia10(View view){
        chartView.setAxisMax(10.0F);
    }
    public void setDistancia5(View view){
        chartView.setAxisMax(5.0F);
    }
    public void setDistancia3(View view){
        chartView.setAxisMax(3.0F);
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {

                runOnUiThread(new Runnable() {
                    public void run() {
                        chartView.addOrReplace("1",0.0f );
                        chartView.addOrReplace("2",0.0f );
                        chartView.addOrReplace("3",0.0f );
                        chartView.addOrReplace("4",0.0f );
                        chartView.addOrReplace("5",0.0f );
                        chartView.addOrReplace("6",0.0f );
                        chartView.addOrReplace("7",0.0f );
                        chartView.addOrReplace("8",0.0f );
                        chartView.addOrReplace("9",0.0f );

                    }
                });
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
}
