package br.com.qpainformatica.microloc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Evento;
import br.com.qpainformatica.microloc.ui.fragments.BeaconFragment;
import br.com.qpainformatica.microloc.ui.fragments.EventFragment;
import br.com.qpainformatica.microloc.ui.fragments.dummy.DummyContent;

public class EventActivity extends AppCompatActivity implements EventFragment.OnListFragmentInteractionListener {

    private Intent intent;
    public final static int NEW_REGISTER=3;
    public final static int EDIT_REGISTER=4;
    public final static String REGISTER="register";
    public final static String IDEVENTO="idevento";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        intent = new Intent(this,EditEventActivity.class);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra(REGISTER,NEW_REGISTER);
                startActivityForResult(intent,NEW_REGISTER);
            }
        });
    }

    @Override
    public void onListFragmentInteraction(Evento item) {
        intent.putExtra(REGISTER,EDIT_REGISTER);
        intent.putExtra(IDEVENTO,item.getId());
        startActivityForResult(intent,NEW_REGISTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EventFragment eventFrag = (EventFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragmentEvent);

        if (eventFrag != null) {
            eventFrag.notifyDataSetChange();
        }else{
            Log.d("NOTIFICACAO","Nullo !");
        }

    }
}
