package br.com.qpainformatica.microloc.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;

import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.File;
import java.io.IOException;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.User;
import br.com.qpainformatica.microloc.domain.util.Base64Util;
import br.com.qpainformatica.microloc.domain.util.CpfCnpjMasks;
import br.com.qpainformatica.microloc.domain.util.ImageInputHelper;
import br.com.qpainformatica.microloc.domain.util.Util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserActivity extends AppCompatActivity implements ImageInputHelper.ImageActionListener {

    @BindView(R.id.editTextNome)
    public EditText editTextNome;
    @BindView(R.id.editTextCPF)
    public EditText editTextCPF;
    @BindView(R.id.editTextRA)
    public EditText editTextRA;
    private ImageInputHelper imageInputHelper;
    @BindView(R.id.fotoUser)
    CircularImageView circularImageViewUser;

    String nome,cpf,ra;
    User user=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextCPF.addTextChangedListener(CpfCnpjMasks.insert(editTextCPF));

        user = User.findById(User.class, 1L);
        if(user==null){
            user = new User();
        }else{
            editTextNome.setText(user.getNome());
            editTextCPF.setText(user.getNumeroDocumento());
            editTextRA.setText(user.getRegistroAluno());
            if(user.getFoto()!=null && !user.getFoto().equals("")){
                circularImageViewUser.setImageBitmap(Base64Util.decodeBase64(user.getFoto()));
            }
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(validateForm()){
                    Snackbar.make(view, "Usuário salvo com sucesso!", Snackbar.LENGTH_LONG)
                            .setAction("Mensagem", null).show();
                    user.setNome(nome);
                    user.setNumeroDocumento(cpf);
                    user.setRegistroAluno(ra);
                    user.save();
                    finish();
                }


            }
        });
        imageInputHelper = new ImageInputHelper(this);
        imageInputHelper.setImageActionListener(this);
    }


    private boolean validateForm(){

        boolean valid = true;

        nome = editTextNome.getText().toString().trim();
        cpf = editTextCPF.getText().toString();
        ra = editTextRA.getText().toString().trim();


        editTextNome.setError(null);
        editTextCPF.setError(null);


        if(nome.isEmpty()){
            editTextNome.setError("nome não pode estar vazio");
            valid = false;
        } else
        if(nome.length()<=3){
            editTextNome.setError("nome deve ter mais de 3 caracteres");
            valid = false;
        } else
        if(nome.indexOf(" ")==-1){
            editTextNome.setError("por favor forneça nome completo");
            valid = false;
        } else
        if(cpf.isEmpty()){
            editTextCPF.setError("cpf não pode estar vazio");
            valid = false;
        } else
        if(cpf.length()!=14){
            editTextCPF.setError("cpf deve ter 11 números");
            valid = false;
        } else
        if(!Util.isCPF(cpf)){
            editTextCPF.setError("cpf inválido");
            valid = false;
        }
        return valid;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageInputHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onImageSelectedFromGallery(Uri uri, File imageFile) {
        imageInputHelper.requestCropImage(uri, 100, 100, 1, 1);
    }

    @Override
    public void onImageTakenFromCamera(Uri uri, File imageFile) {
        imageInputHelper.requestCropImage(uri, 100, 100, 1, 1);
    }

    @Override
    public void onImageCropped(Uri uri, File imageFile) {
        try {

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            user.setFoto(Base64Util.encodeTobase64(bitmap));
            circularImageViewUser.setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void onClickFromCamera(View v){
        imageInputHelper.takePhotoWithCamera();
    }


    public void onClickFromGalery(View v){
        imageInputHelper.selectImageFromGallery();
    }


}
