package br.com.qpainformatica.microloc.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.File;
import java.io.IOException;
import java.util.List;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Evento;
import br.com.qpainformatica.microloc.domain.model.Local;
import br.com.qpainformatica.microloc.domain.util.Base64Util;
import br.com.qpainformatica.microloc.domain.util.DateMask;
import br.com.qpainformatica.microloc.domain.util.ImageInputHelper;
import br.com.qpainformatica.microloc.domain.util.Mask;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

public class EditEventActivity extends AppCompatActivity implements ImageInputHelper.ImageActionListener {

    @BindView(R.id.spinnerEvent)
    MaterialSpinner spinner;
    @BindView(R.id.editTextTitulo)
    EditText editTextTitulo;
    @BindView(R.id.editTextDescricao)
    EditText editTextDescricao;
    @BindView(R.id.editTextDataInicio)
    EditText editTextDataInicio;
    @BindView(R.id.editTextDataFinal)
    EditText editTextDataFinal;
    @BindView(R.id.editTextHoraInicio)
    EditText editTextHoraInicio;
    @BindView(R.id.editTextHoraFinal)
    EditText editTextHoraFinal;
    @BindView(R.id.buttonApagar)
    Button buttonDeletar;

    private ImageInputHelper imageInputHelper;
    @BindView(R.id.fotoEvento)
    CircularImageView circularImageViewUser;

    Intent intent;
    Evento evento;
    Long idEvento;

    private String titulo, descricao, dataInicio, dataFinal, horaInicio,horaFinal,localIdentificacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        ButterKnife.bind(this);

        buttonDeletar.setEnabled(false);
        buttonDeletar.setVisibility(View.INVISIBLE);
        String compareValue=null;


        intent = getIntent();
        int acao = intent.getIntExtra(EventActivity.REGISTER,0);



        if(acao==EventActivity.NEW_REGISTER){
            evento = new Evento();
        }else if(acao==EventActivity.EDIT_REGISTER){
            idEvento = intent.getLongExtra(EventActivity.IDEVENTO,0L);
            Log.d("NOTIFICACAO","IDBEACON:"+idEvento);
            evento = Evento.findById(Evento.class,idEvento);
            editTextTitulo.setText(evento.getTitulo());
            editTextDescricao.setText(evento.getDescricao());
            editTextDataInicio.setText(evento.getDataInicio());
            editTextDataFinal.setText(evento.getDataFinal());
            editTextHoraInicio.setText(evento.getHoraInicio());
            editTextHoraFinal.setText(evento.getHoraFinal());
            if(evento.getFoto()!=null && !evento.getFoto().equals("")){
                circularImageViewUser.setImageBitmap(Base64Util.decodeBase64(evento.getFoto()));
            }
            long idLocal = evento.getIdLocal();
            Local local = Local.findById(Local.class,idLocal);
            compareValue=local.getIdentificacao();
            buttonDeletar.setVisibility(View.VISIBLE);
            buttonDeletar.setEnabled(true);
        }

        Log.d("SPINNERAAA","EVENTO: compared value: "+compareValue);



        String[] ITEMS;
        int contador=0;
        List<Local> lista = Local.listAll(Local.class);
        if(lista!=null && lista.size()>0){
            ITEMS = new String[lista.size()];
            for(Local l: lista){
                ITEMS[contador++]=l.getIdentificacao();
            }
        }else{
            ITEMS = new String[1];
            ITEMS[0]="Local não cadastrado!";
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (compareValue!=null) {
            int spinnerPosition = adapter.getPosition(compareValue);
            spinner.setSelection(spinnerPosition);
        }


        editTextDataInicio.addTextChangedListener(DateMask.insert(editTextDataInicio));
        editTextDataFinal.addTextChangedListener(DateMask.insert(editTextDataFinal));
        editTextHoraInicio.addTextChangedListener(Mask.insert("##:##", editTextHoraInicio));
        editTextHoraFinal.addTextChangedListener(Mask.insert("##:##", editTextHoraFinal));

        imageInputHelper = new ImageInputHelper(this);
        imageInputHelper.setImageActionListener(this);
    }

    public void deletar(View view){

        new MaterialStyledDialog.Builder(this)
                .setTitle("Atenção!")
                .setDescription("Deseja relamente delelar este registro?")
                //.setHeaderDrawable(R.drawable.header)
                .setStyle(Style.HEADER_WITH_ICON)
                .withDarkerOverlay(false)
                .setIcon(R.drawable.atention)
                //.setHeaderDrawable(R.drawable.atention)
                .withDialogAnimation(true)
                .setCancelable(true)
                .setNegativeText("Não")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .setPositiveText("Sim")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        evento.delete();
                        returnResult();
                    }
                })
                .show();


    }

    public void returnResult(){
        Log.d("NOTIFICACAO","SAIUDAQUI");
        setResult(RESULT_OK,intent);
        finish();
    }

    public void salvar(View view){

        if(validateForm()) {
            evento.setTitulo(titulo);
            evento.setDescricao(descricao);
            evento.setDataInicio(dataInicio);
            evento.setDataFinal(dataFinal);
            evento.setHoraInicio(horaInicio);
            evento.setHoraFinal(horaFinal);
            Local local = Local.find(Local.class,"identificacao = '"+localIdentificacao+"'").get(0);
            if(local!=null){
                evento.setIdLocal(local.getId());
            }
            Log.d("EVENTOAQUI",""+evento.toString());
            evento.save();
            new MaterialStyledDialog.Builder(this)
                    .setTitle("Sucesso!")
                    .setDescription("Registro salvo com sucesso!")
                    //.setHeaderDrawable(R.drawable.header)
                    .setStyle(Style.HEADER_WITH_ICON)
                    .withDarkerOverlay(false)
                    .setIcon(R.drawable.sucess)
                    //.setHeaderDrawable(R.drawable.sucess)
                    .withDialogAnimation(true)
                    .setCancelable(true)
                    .setPositiveText("OK")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            returnResult();
                        }
                    })
                    .show();
        }
    }

    private boolean validateForm(){

        boolean valid = true;

        titulo = editTextTitulo.getText().toString();
        descricao = editTextDescricao.getText().toString();
        dataInicio = editTextDataInicio.getText().toString();
        dataFinal = editTextDataFinal.getText().toString();
        horaInicio = editTextHoraInicio.getText().toString();
        horaFinal = editTextHoraFinal.getText().toString();
        String item = (String)spinner.getSelectedItem();
        if(item!=null){
            localIdentificacao = spinner.getSelectedItem().toString();
        }else{
            localIdentificacao="";
        }


        editTextTitulo.setError(null);
        editTextDescricao.setError(null);
        editTextDataInicio.setError(null);
        editTextDataFinal.setError(null);
        editTextHoraInicio.setError(null);
        editTextHoraFinal.setError(null);
        spinner.setError(null);

        if(titulo.isEmpty()){
            editTextTitulo.setError("titulo não pode estar vazia");
            valid = false;
        } else
        if(editTextTitulo.length()<=5){
            editTextTitulo.setError("titulo deve ter mais de 5 caracteres");
            valid = false;
        }  else
        if(descricao.isEmpty()){
            editTextDescricao.setError("descrição não pode estar vazia");
            valid = false;
        }else
        if(dataInicio.isEmpty()){
            editTextDataInicio.setError("data de inicio não pode estar vazia");
            valid = false;
        }else
        if(dataFinal.isEmpty()){
            editTextDataFinal.setError("data de final não pode estar vazia");
            valid = false;
        }else
        if(horaInicio.isEmpty()){
            editTextHoraInicio.setError("horário de início não pode estar vazio");
            valid = false;
        }else
        if(horaFinal.isEmpty()){
            editTextHoraFinal.setError("horário final não pode estar vazio");
            valid = false;
        } else
        if(localIdentificacao==null || localIdentificacao.equals("") || localIdentificacao.equals("Local não cadastrado!")){
            spinner.setError("Um local deve ser selecionado para este evento");
            valid = false;
        }


        return valid;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageInputHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onImageSelectedFromGallery(Uri uri, File imageFile) {
        imageInputHelper.requestCropImage(uri, 150, 150, 1, 1);
    }

    @Override
    public void onImageTakenFromCamera(Uri uri, File imageFile) {
        imageInputHelper.requestCropImage(uri, 150, 150, 1, 1);
    }

    @Override
    public void onImageCropped(Uri uri, File imageFile) {
        try {

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            evento.setFoto(Base64Util.encodeTobase64(bitmap));
            circularImageViewUser.setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void onClickFromCamera(View v){
        imageInputHelper.takePhotoWithCamera();
    }


    public void onClickFromGalery(View v){
        imageInputHelper.selectImageFromGallery();
    }

}
