package br.com.qpainformatica.microloc.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

import java.util.List;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Beacon;
import br.com.qpainformatica.microloc.domain.model.Local;
import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

public class EditLocalActivity extends AppCompatActivity {


    @BindView(R.id.spinnerBeacon)
    MaterialSpinner spinner;
    @BindView(R.id.editTextIdentificacao)
    public EditText editTextIdentificacao;
    @BindView(R.id.editTextDescricao)
    public EditText editTextDescricao;
    @BindView(R.id.editTextEndereco)
    public EditText editTextEndereco;
    @BindView(R.id.editTextIES)
    public EditText editTextIES;
    @BindView(R.id.editTextCampus)
    public EditText editTextCampus;
    @BindView(R.id.editTextCapacidade)
    public EditText editTextCapacidade;
    @BindView(R.id.buttonApagar)
    Button buttonDeletar;

    private String identificacao, descricao, endereco, ies, campus,capacidade,beaconIdentificacao;

    private Local local;
    private long idLocal=0;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_local);
        ButterKnife.bind(this);

        buttonDeletar.setEnabled(false);
        buttonDeletar.setVisibility(View.INVISIBLE);
        String compareValue=null;

        intent = getIntent();
        int acao = intent.getIntExtra(LocalActivity.REGISTER,0);


        if(acao==LocalActivity.NEW_REGISTER){
            local = new Local();
        }else if(acao==LocalActivity.EDIT_REGISTER) {
            idLocal = intent.getLongExtra(LocalActivity.IDLOCAL,0L);
            Log.d("NOTIFICACAO","IDBEACON:"+idLocal);
            local = Local.findById(Local.class,idLocal);
            editTextIdentificacao.setText(local.getIdentificacao());
            editTextDescricao.setText(local.getDescricao());
            editTextCampus.setText(local.getCampus());
            editTextCapacidade.setText(local.getCapacidade());
            editTextEndereco.setText(local.getEndereco());
            editTextIES.setText(local.getIes());
            long idBeacon = local.getIdBeacon();
            Beacon beacon = Beacon.findById(Beacon.class,idBeacon);
            compareValue=beacon.getIdentification();
            buttonDeletar.setVisibility(View.VISIBLE);
            buttonDeletar.setEnabled(true);
        }
        Log.d("SPINNERAAA","compared value: "+compareValue);

        String[] ITEMS;
        int contador=0;
        List<Beacon> lista = Beacon.listAll(Beacon.class);
        if(lista!=null && lista.size()>0){
            ITEMS = new String[lista.size()];
            for(Beacon b: lista){
                ITEMS[contador++]=b.getIdentification();
            }
        }else{
            ITEMS = new String[1];
            ITEMS[0]="Beacon não cadastrado!";
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (compareValue!=null) {
            int spinnerPosition = adapter.getPosition(compareValue);
            spinner.setSelection(spinnerPosition);
        }
    }

    public void deletar(View view){

        new MaterialStyledDialog.Builder(this)
                .setTitle("Atenção!")
                .setDescription("Deseja relamente delelar este registro?")
                //.setHeaderDrawable(R.drawable.header)
                .setStyle(Style.HEADER_WITH_ICON)
                .withDarkerOverlay(false)
                .setIcon(R.drawable.atention)
                //.setHeaderDrawable(R.drawable.atention)
                .withDialogAnimation(true)
                .setCancelable(true)
                .setNegativeText("Não")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .setPositiveText("Sim")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        local.delete();
                        returnResult();
                    }
                })
                .show();


    }

    public void returnResult(){
        Log.d("NOTIFICACAO","SAIUDAQUI");
        setResult(RESULT_OK,intent);
        finish();
    }

    public void salvar(View view){

        Log.d("LOCALAQUI","Chegou aqui !");

        if(validateForm()) {
            Log.d("LOCALAQUI","VALIDO!");
            local.setIdentificacao(identificacao);
            local.setCampus(campus);
            local.setCapacidade(capacidade);
            local.setDescricao(descricao);
            local.setEndereco(endereco);
            local.setIes(ies);
            Beacon beacon = Beacon.find(Beacon.class,"identification = '"+beaconIdentificacao+"'").get(0);
            if(beacon!=null){
                Log.d("LOCALAQUI","acou beacon");
                local.setIdBeacon(beacon.getId());
            }

            local.save();
            new MaterialStyledDialog.Builder(this)
                    .setTitle("Sucesso!")
                    .setDescription("Registro salvo com sucesso!")
                    //.setHeaderDrawable(R.drawable.header)
                    .setStyle(Style.HEADER_WITH_ICON)
                    .withDarkerOverlay(false)
                    .setIcon(R.drawable.sucess)
                    //.setHeaderDrawable(R.drawable.sucess)
                    .withDialogAnimation(true)
                    .setCancelable(true)
                    .setPositiveText("OK")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            returnResult();
                        }
                    })
                    .show();
        }
    }

    private boolean validateForm(){

        boolean valid = true;

        identificacao = editTextIdentificacao.getText().toString();
        descricao = editTextDescricao.getText().toString();
        endereco = editTextEndereco.getText().toString();
        ies = editTextIES.getText().toString();
        campus = editTextCampus.getText().toString();
        capacidade = editTextCapacidade.getText().toString();
        String item = (String)spinner.getSelectedItem();
        if(item!=null){
            beaconIdentificacao = spinner.getSelectedItem().toString();
        }else{
            beaconIdentificacao="";
        }


        editTextIdentificacao.setError(null);
        editTextDescricao.setError(null);
        editTextEndereco.setError(null);
        editTextIES.setError(null);
        editTextCampus.setError(null);
        editTextCapacidade.setError(null);
        spinner.setError(null);

        if(identificacao.isEmpty()){
            editTextIdentificacao.setError("identifação não pode estar vazia");
            valid = false;
        } else
        if(identificacao.length()<=5){
            editTextIdentificacao.setError("identifação deve ter mais de 5 caracteres");
            valid = false;
        } else
        if(identificacao.indexOf(" ")>-1){
            editTextIdentificacao.setError("identificação não pode conter espaços em branco");
            valid = false;
        } else
        if(descricao.isEmpty()){
            editTextDescricao.setError("descrição não pode estar vazia");
            valid = false;
        } else
        if(beaconIdentificacao==null || beaconIdentificacao.equals("") || beaconIdentificacao.equals("Beacon não cadastrado!")){
            spinner.setError("Um beacon deve ser selecionado para e te local");
            valid = false;
        }

        Log.d("LOCALAQUI","valid="+valid);

        return valid;

    }
}
