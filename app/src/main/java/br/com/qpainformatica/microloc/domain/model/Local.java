package br.com.qpainformatica.microloc.domain.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by elcio on 16/11/17.
 */

public class Local extends SugarRecord implements Serializable {

    private String identificacao;
    private String descricao;
    private String endereco;
    private String ies;
    private String campus;
    private String capacidade;
    private long idBeacon;

    public String getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getIes() {
        return ies;
    }

    public void setIes(String ies) {
        this.ies = ies;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(String capacidade) {
        this.capacidade = capacidade;
    }

    public long getIdBeacon() {
        return idBeacon;
    }

    public void setIdBeacon(long idBeacon) {
        this.idBeacon = idBeacon;
    }
}
