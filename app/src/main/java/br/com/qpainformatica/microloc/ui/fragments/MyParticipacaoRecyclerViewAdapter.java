package br.com.qpainformatica.microloc.ui.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Beacon;
import br.com.qpainformatica.microloc.domain.model.Evento;
import br.com.qpainformatica.microloc.domain.model.Participacao;
import br.com.qpainformatica.microloc.domain.model.User;
import br.com.qpainformatica.microloc.domain.util.Base64Util;
import br.com.qpainformatica.microloc.ui.fragments.ParticipacaoFragment.OnListFragmentInteractionListener;
import br.com.qpainformatica.microloc.ui.fragments.dummy.DummyContent.DummyItem;

import java.util.List;


public class MyParticipacaoRecyclerViewAdapter extends RecyclerView.Adapter<MyParticipacaoRecyclerViewAdapter.ViewHolder> {

    private final List<Participacao> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyParticipacaoRecyclerViewAdapter(List<Participacao> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_participacao, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        User user = User.findById(User.class,1L);
        Evento evento = Evento.findById(Evento.class,mValues.get(position).getIdEvento());

        holder.textViewTitulo.setText(evento.getTitulo());
        holder.textViewDataHora.setText(evento.getDataInicio()+" "+evento.getHoraInicio());
        holder.userPhoto.setImageBitmap(Base64Util.decodeBase64(user.getFoto()));
        holder.eventPhoto.setImageBitmap(Base64Util.decodeBase64(evento.getFoto()));

//        holder.mIdView.setText(mValues.get(position).id);
//        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView textViewTitulo;
        public final TextView textViewDataHora;
        public final CircularImageView userPhoto;
        public final CircularImageView eventPhoto;
        public Participacao mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            textViewTitulo = (TextView) view.findViewById(R.id.textViewTitulo);
            textViewDataHora = (TextView) view.findViewById(R.id.textViewDataHora);
            userPhoto = (CircularImageView)view.findViewById(R.id.userPhoto);
            eventPhoto = (CircularImageView)view.findViewById(R.id.eventPhoto);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public void updateList(List<Participacao> newlist) {
        mValues.clear();
        mValues.addAll(newlist);
        this.notifyDataSetChanged();
    }
}
