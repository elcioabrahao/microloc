package br.com.qpainformatica.microloc.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Beacon;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditBeaconActivity extends AppCompatActivity {


    @BindView(R.id.editTextIdentificacao)
    public EditText editTextIdentificacao;
    @BindView(R.id.editTextMaior)
    public EditText editTextMaior;
    @BindView(R.id.editTextMenor)
    public EditText editTextMenor;
    @BindView(R.id.buttonApagar)
    Button buttonDeletar;

    private String identificacao, maior, menor;

    private Beacon beacon;
    private long idBeacon=0;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_beacon);
        ButterKnife.bind(this);

        buttonDeletar.setEnabled(false);
        buttonDeletar.setVisibility(View.INVISIBLE);


        intent = getIntent();
        int acao = intent.getIntExtra(BeaconActivity.REGISTER,0);

        Log.d("BEACONAAA","Acao:"+acao);



        if(acao==BeaconActivity.NEW_REGISTER){
            beacon = new Beacon();

        }else  if(acao==2){
            idBeacon = intent.getLongExtra(BeaconActivity.IDBEACON,0L);
            Log.d("BEACONAAA","IDBEACON:"+idBeacon);
            beacon = Beacon.findById(Beacon.class,idBeacon);
            editTextIdentificacao.setText(beacon.getIdentification());
            editTextMaior.setText(beacon.getMaior());
            editTextMenor.setText(beacon.getMenor());
            buttonDeletar.setVisibility(View.VISIBLE);
            buttonDeletar.setEnabled(true);
        }

    }

    public void deletar(View view){

        new MaterialStyledDialog.Builder(this)
                .setTitle("Atenção!")
                .setDescription("Deseja relamente delelar este registro?")
                //.setHeaderDrawable(R.drawable.header)
                .setStyle(Style.HEADER_WITH_ICON)
                .withDarkerOverlay(false)
                .setIcon(R.drawable.atention)
                //.setHeaderDrawable(R.drawable.atention)
                .withDialogAnimation(true)
                .setCancelable(true)
                .setNegativeText("Não")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .setPositiveText("Sim")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        beacon.delete();
                        returnResult();
                    }
                })
                .show();


    }

    public void returnResult(){
        Log.d("NOTIFICACAO","SAIUDAQUI");
        setResult(RESULT_OK,intent);
        finish();
    }

    public void salvar(View view){

        if(validateForm()) {
            beacon.setIdentification(identificacao);
            beacon.setMaior(maior);
            beacon.setMenor(menor);
            beacon.save();
            new MaterialStyledDialog.Builder(this)
                    .setTitle("Sucesso!")
                    .setDescription("Registro salvo com sucesso!")
                    //.setHeaderDrawable(R.drawable.header)
                    .setStyle(Style.HEADER_WITH_ICON)
                    .withDarkerOverlay(false)
                    .setIcon(R.drawable.sucess)
                    //.setHeaderDrawable(R.drawable.sucess)
                    .withDialogAnimation(true)
                    .setCancelable(true)
                    .setPositiveText("OK")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            returnResult();
                        }
                    })
                    .show();
        }
    }

    private boolean validateForm(){

        boolean valid = true;

        identificacao = editTextIdentificacao.getText().toString();
        maior = editTextMaior.getText().toString();
        menor = editTextMenor.getText().toString();


        editTextIdentificacao.setError(null);
        editTextMaior.setError(null);
        editTextMenor.setError(null);


        if(identificacao.isEmpty()){
            editTextIdentificacao.setError("identifação não pode estar vazia");
            valid = false;
        } else
        if(identificacao.length()<=5){
            editTextIdentificacao.setError("identifação deve ter mais de 5 caracteres");
            valid = false;
        } else
        if(identificacao.indexOf(" ")>-1){
            editTextIdentificacao.setError("identificação não pode conter espaços em branco");
            valid = false;
        } else
        if(maior.isEmpty()){
            editTextMaior.setError("maior identificador extra não pode estar vazio");
            valid = false;
        } else
        if(menor.isEmpty()){
            editTextMenor.setError("menor identificador extra não pode estar vazio");
            valid = false;
        }


        return valid;

    }


}
