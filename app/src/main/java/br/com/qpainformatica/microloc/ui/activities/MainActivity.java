package br.com.qpainformatica.microloc.ui.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.ramotion.foldingcell.FoldingCell;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;
import java.util.List;

import br.com.qpainformatica.microloc.MicrolocApplication;
import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Evento;
import br.com.qpainformatica.microloc.domain.model.Local;
import br.com.qpainformatica.microloc.domain.model.Participacao;
import br.com.qpainformatica.microloc.domain.util.Util;
import br.com.qpainformatica.microloc.ui.fragments.LocalFragment;
import br.com.qpainformatica.microloc.ui.fragments.ParticipacaoFragment;
import br.com.qpainformatica.microloc.ui.fragments.dummy.DummyContent;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,  ParticipacaoFragment.OnListFragmentInteractionListener, BeaconConsumer {

    protected static final String TAG = "MainActivity";
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);


    //    @BindView(R.id.monitoringText)
//    EditText editTextMonitoringText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        beaconManager.bind(this);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RadarActivity.class);
                startActivity(intent);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        verifyBluetooth();
        logToDisplay("Application just launched");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Este aplicativo precisa de acesso a localização.");
                builder.setMessage("Por favor conceda acesso a lozalização pois este app precisa da autorização para localizar beacons em background.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    @TargetApi(23)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                PERMISSION_REQUEST_COARSE_LOCATION);
                    }

                });
                builder.show();
            }
        }



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent intent = new Intent(this,RangingActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.action_clear){
            Participacao.deleteAll(Participacao.class);
            notifyDataSetChange();
        }else if(id == R.id.action_sobre){
            new MaterialStyledDialog.Builder(this)
                    .setTitle("Microlocalização")
                    .setDescription("Prof.Dr.Elcio Abrahão, um projeto em conjunto com o Grupo Anima Educação, Novembro 2017, todos os diretos reservados.")
                    //.setHeaderDrawable(R.drawable.header)
                    .setStyle(Style.HEADER_WITH_ICON)
                    .withDarkerOverlay(false)
                    .setIcon(R.drawable.sucess)
                    //.setHeaderDrawable(R.drawable.sucess)
                    .withDialogAnimation(true)
                    .setCancelable(true)
                    .setPositiveText("OK")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent = null;

        if (id == R.id.nav_user) {
            intent = new Intent(this,UserActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_beacon) {
            intent = new Intent(this,BeaconActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_local) {
            intent = new Intent(this,LocalActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_event) {
            intent = new Intent(this,EventActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_participacao) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Funcionamento limitado");
                    builder.setMessage("Uma vez que a permissão de localização não foi concedida, este app" +
                            "não será capaz de localizar beacons quando em background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }

    public void onRangingClicked(View view) {
        Intent myIntent = new Intent(this, RangingActivity.class);
        this.startActivity(myIntent);
    }


    private void verifyBluetooth() {

        try {
            if (!BeaconManager.getInstanceForApplication(this).checkAvailability()) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Bluetooth desabilitado");
                builder.setMessage("Por favor habilite o bluetooth nas configurações e restarte esta aplicação.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                        System.exit(0);
                    }
                });
                builder.show();
            }
        }
        catch (RuntimeException e) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Bluetooth LE não disponível");
            builder.setMessage("Desculpe, este aparelho não suporta Bluetooth LE.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                    System.exit(0);
                }

            });
            builder.show();

        }

    }

    @Override
    public void onListFragmentInteraction(Participacao item) {
        //fazer nada por enquanto
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((MicrolocApplication) this.getApplicationContext()).setMonitoringActivity(null);
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((MicrolocApplication) this.getApplicationContext()).setMonitoringActivity(this);
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(false);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {

                for(Beacon b: beacons){
                    Log.d("BEACONSAAA","Entrou");

                    List<br.com.qpainformatica.microloc.domain.model.Beacon> blLista = br.com.qpainformatica.microloc.domain.model.Beacon.findWithQuery(br.com.qpainformatica.microloc.domain.model.Beacon.class,"SELECT * from Beacon WHERE identification = '"+b.getId1().toString()+"'");
                    if(blLista.size()>0){
                        br.com.qpainformatica.microloc.domain.model.Beacon bl=blLista.get(0);
                        List<Local> lLista = Local.findWithQuery(Local.class,"SELECT * FROM Local WHERE id_beacon = "+bl.getId());
                        if(lLista.size()>0){
                            Local l = lLista.get(0);
                            List<Evento> eLista = Evento.findWithQuery(Evento.class,"SELECT * FROM Evento WHERE id_local = "+l.getId());
                            if(eLista.size()>0){
                                Evento e = eLista.get(0);
                                Log.d("BEACONSAAA","Achou o evento");
                                Log.d("BEACONSAAA","DataInicio: "+e.getDataInicio());
                                Log.d("BEACONSAAA","HoraInicio: "+e.getHoraInicio());
                                Log.d("BEACONSAAA","DataFinal: "+e.getDataFinal());
                                Log.d("BEACONSAAA","HoraFinal: "+e.getHoraFinal());
                                Log.d("BEACONSAAA","Data Hoje: "+Util.getTodayDate());


                                if(Util.onEventRange(e.getDataInicio(),e.getHoraInicio(),e.getDataFinal(),e.getHoraFinal())) {

                                    Log.d("BEACONSAAA","Esta no range");

                                    List<Participacao> lista = Participacao.findWithQuery(Participacao.class,"SELECT * FROM participacao WHERE id_evento = "+e.getId());

                                    if(lista.size()==0) {

                                        Log.d("BEACONSAAA","Registro é o primeiro");

                                        Participacao p = new Participacao();
                                        p.setIdEvento(e.getId());
                                        p.setIdUser(1L);
                                        p.save();
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                notifyDataSetChange();
                                            }
                                        });
                                    }
                                }

                            }
                        }
                    }

                }


            }

        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {   }
    }

    public void logToDisplay(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
                //EditText editText = (EditText)MainActivity.this.findViewById(R.id.rangingText);
                //editText.append(line+"\n");
                //editTextMonitoringText.append(line+"\n");
                Log.d("BEACON",""+line);

            }
        });
    }


    private void notifyDataSetChange(){

        ParticipacaoFragment partFrag = (ParticipacaoFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragmentParticipacao);

        if (partFrag != null) {
            partFrag.notifyDataSetChange();
        }else{
            Log.d("NOTIFICACAO","Nullo !");
        }
    }
}
