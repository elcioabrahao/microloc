package br.com.qpainformatica.microloc.ui.fragments;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Beacon;
import br.com.qpainformatica.microloc.domain.model.Evento;
import br.com.qpainformatica.microloc.ui.fragments.EventFragment.OnListFragmentInteractionListener;
import br.com.qpainformatica.microloc.ui.fragments.dummy.DummyContent.DummyItem;

import java.util.List;


public class MyEventRecyclerViewAdapter extends RecyclerView.Adapter<MyEventRecyclerViewAdapter.ViewHolder> {

    private final List<Evento> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyEventRecyclerViewAdapter(List<Evento> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_event, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getTitulo());
        holder.mDataInicioView.setText(mValues.get(position).getDataInicio()+" "+mValues.get(position).getHoraInicio());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mDataInicioView;
        public Evento mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.titulo);
            mDataInicioView = (TextView) view.findViewById(R.id.dataHoraInicio);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDataInicioView.getText() + "'";
        }
    }

    public void updateList(List<Evento> newlist) {
        Log.d("EVENTOAQUI","tamanho:"+newlist.size());
        for(Evento e: newlist){
            Log.d("EVENTOAQUI",""+e.toString());
        }
        mValues.clear();
        mValues.addAll(newlist);
        this.notifyDataSetChanged();
    }
}
