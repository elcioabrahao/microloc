package br.com.qpainformatica.microloc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import br.com.qpainformatica.microloc.R;
import br.com.qpainformatica.microloc.domain.model.Beacon;
import br.com.qpainformatica.microloc.ui.fragments.BeaconFragment;
import br.com.qpainformatica.microloc.ui.fragments.dummy.DummyContent;

public class BeaconActivity extends AppCompatActivity implements BeaconFragment.OnListFragmentInteractionListener {


    private Intent intent;
    public final static int NEW_REGISTER=1;
    public final static int EDIT_REGISTER=2;
    public final static String REGISTER="register";
    public final static String IDBEACON="idbeacon";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            intent = new Intent(BeaconActivity.this,EditBeaconActivity.class);
            intent.putExtra(REGISTER,NEW_REGISTER);
            startActivityForResult(intent,NEW_REGISTER);
            }
        });
    }

    @Override
    public void onListFragmentInteraction(Beacon item) {
        intent = new Intent(BeaconActivity.this,EditBeaconActivity.class);
        intent.putExtra(REGISTER,EDIT_REGISTER);
        intent.putExtra(IDBEACON,item.getId());
        startActivityForResult(intent,NEW_REGISTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("NOTIFICACAO","Chegou aqui");

        BeaconFragment beaconFrag = (BeaconFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragmentBeacon);

        if (beaconFrag != null) {
            beaconFrag.notifyDataSetChange();
        }else{
            Log.d("NOTIFICACAO","Nullo !");
        }

    }




}
